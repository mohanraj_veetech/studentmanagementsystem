package com.mohanraj.studentmanagement.main;

			

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


/**
 * @author V.MOHANRAJ
 * The Class StudentManagementSystem.
 */
@SpringBootApplication
public class StudentManagementSystem extends SpringBootServletInitializer{

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(StudentManagementSystem.class, args);
	}



}

