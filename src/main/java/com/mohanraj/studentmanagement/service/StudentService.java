package com.mohanraj.studentmanagement.service;

import java.util.List;
import java.util.Optional;

import com.mohanraj.studentmanagement.model.Student;

/**
 * @author V.MOHANRAJ
 * 
 * The Interface StudentService.
 */
public interface StudentService {

	/**
	 * Creates the student.
	 *
	 * @param student the student
	 * @return the student
	 */
	Student createStudent(Student student);

	/**
	 * Gets the student by id.
	 *
	 * @param studentId the student id
	 * @return the student by id
	 */
	Optional<Student> getStudentById(Long studentId);

	/**
	 * Update student.
	 *
	 * @param student the student
	 * @return the student
	 */
	Student updateStudent(Student student);

	/**
	 * Delete student.
	 *
	 * @param student the student
	 */
	void deleteStudent(Student student);

	/**
	 * Gets the all students.
	 *
	 * @return the all students
	 */
	List<Student> getAllStudents();

}
