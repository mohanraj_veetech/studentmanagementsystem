package com.mohanraj.studentmanagement.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mohanraj.studentmanagement.dao.StudentDao;
import com.mohanraj.studentmanagement.model.Student;


/**
 * @author V.MOHANRAJ
 * 
 * The Class StudentServiceImpl.
 */
@Service
public class StudentServiceImpl implements StudentService {

	/** The student dao. */
	@Autowired
	StudentDao studentDao;

	/* 
	 * Create Student
	 */
	@Override
	public Student createStudent(Student student) {

		return studentDao.save(student);
	}

	/* 
	 * Get Individual student
	 */
	@Override
	public Optional<Student> getStudentById(Long studentId) {

		return studentDao.findById(studentId);
	}

	/* 
	 * Update student
	 */
	@Override
	public Student updateStudent(Student student) {

		return studentDao.save(student);
	}

	/* 
	 * Delete student
	 */
	@Override
	public void deleteStudent(Student student) {

		studentDao.delete(student);

	}

	/* 
	 * View All Students
	 */
	@Override
	public List<Student> getAllStudents() {

		return studentDao.findAll();
	}

}
