package com.mohanraj.studentmanagement.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mohanraj.studentmanagement.model.Student;
import com.mohanraj.studentmanagement.service.StudentService;

/**
 * The Class StudentController.
 *
 * @author V.MOHANRAJ
 * 
 *         The Class StudentController.
 */
@RestController
@RequestMapping("/api")
public class StudentController {

	/** The student service. */
	@Autowired

	StudentService studentService;

	/**
	 * Creates the student.
	 *
	 * @param student
	 *            the student
	 * @return the response entity
	 */
	@PostMapping("/students")
	public ResponseEntity<Object> createStudent(@RequestBody Student student) {
		Student savedStudent = studentService.createStudent(student);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedStudent.getStudentId()).toUri();

		return ResponseEntity.created(location).build();

	}

	/**
	 * Delete student.
	 *
	 * @param id
	 *            the id
	 */
	@DeleteMapping("/students/{id}")
	public void deleteStudent(@PathVariable long id) {
		Optional<Student> studentFromDB = studentService.getStudentById(id);
		if (studentFromDB.isPresent()) {
			studentService.deleteStudent(studentFromDB.get());
		}

	}

	/**
	 * Gets the student.
	 *
	 * @param id the id
	 * @return the student
	 */
	@GetMapping("/students/{id}")
	public Student getStudent(@PathVariable long id) {
		Optional<Student> studentFromDB = studentService.getStudentById(id);
		return studentFromDB.get();

	}

	/**
	 * Update student.
	 *
	 * @param student
	 *            the student
	 * @param id
	 *            the id
	 * @return the response entity
	 */
	@PutMapping("/students/{id}")
	public ResponseEntity<Object> updateStudent(@RequestBody Student student, @PathVariable long id) {

		Optional<Student> studentOptional = studentService.getStudentById(id);

		if (!studentOptional.isPresent())
			return ResponseEntity.notFound().build();

		student.setStudentId(id);

		studentService.updateStudent(student);

		return ResponseEntity.noContent().build();
	}
	
	/**
	 * View all students.
	 *
	 * @return the list
	 */
	@GetMapping("/view-all")
	public List<Student> viewAllStudents()
	{
		return studentService.getAllStudents();
	}

}
