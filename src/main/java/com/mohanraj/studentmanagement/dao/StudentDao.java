package com.mohanraj.studentmanagement.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mohanraj.studentmanagement.model.Student;

/**
 * @author V.MOHANRAJ
 * 
 * The Interface StudentDao.
 */
@Repository
public interface StudentDao extends JpaRepository<Student, Long>{

}
